## Install K8s

* Download Ubuntu virtual box

* Start them in network bridge

* Perform system updates

* Disable swap on virtual boxes by remove swap line from `/etc/fstab`

* Change machine hostname so they are unique in `/etc/hostname`

* Install [docker, kubeadm](https://kubernetes.io/docs/setup/independent/install-kubeadm/)

* Set kubelet cgroup to docker cgroup with: `"KUBELET_AUTHZ_ARGS=--cgroup-driver=systemd --authorization-mode=Webhook --client-ca-file=/etc/kubernetes/pki/ca.crt"` in `/etc/systemd/system/kubelet.service.d/10-kubeadm.conf`

* Reboot

* Init master with: [kubeadm init](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#24-initializing-your-master)

* Install network driver: [weave net](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#pod-network)

* Make the other nodes join the cluster: [kubeadm join](https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#44-joining-your-nodes)


## Enable k8s dashboard

Create an admin user with: https://github.com/kubernetes/dashboard/wiki/Creating-sample-user

Get the token with:

```shell
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

proxy the dashboard on your computer

```shell
 kubectl --kubeconfig ./admin.conf proxy
```

Get dashboard token

```
kubectl --kubeconfig ./admin.conf -n kube-system describe secret $(kubectl -n kube-system --kubeconfig ./admin.conf get secret | grep admin-user | awk '{print $1}')
```

## Resolution DNS

Update /etc/resolv.conf on each server node

```
nameserver 8.8.8.8
```

## Create namespaces

kubectl create -f namespace-breizhcamp1.json

with namespace-breizhcamp1.json

```json
{
  "kind": "Namespace",
  "apiVersion": "v1",
  "metadata": {
    "name": "breizhcamp1",
    "labels": {
      "name": "breizhcamp1"
    }
  }
}
```

Create Users

`kubectl --namespace=breizhcamp1 create serviceaccount breizhcamp-usr1`

