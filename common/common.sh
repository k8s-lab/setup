#!/bin/bash

apt-get update
apt-get install -y apt-transport-https ca-certificates curl software-properties-common
apt-get -y full-upgrade

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-cache policy docker-ce
apt-get install -y docker-ce

apt-get install -y kubelet kubeadm kubectl

echo "installed" > /home/vagrant/common