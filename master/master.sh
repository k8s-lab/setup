#!/bin/bash

echo master > /etc/hostname
hostname master

kubeadm init --apiserver-advertise-address=192.168.10.21

mkdir /home/vagrant/.kube
cp /etc/kubernetes/admin.conf /home/vagrant/.kube/config
chown vagrant:vagrant /home/vagrant/.kube/config

su - vagrant
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
exit

echo "installed" > /home/vagrant/master